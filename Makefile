ARCH?=aarch64
FORMAT?=elf
DEVICE_TARGET?=RASPI

GCC=$(ARCH)-$(FORMAT)-gcc
OBJCOPY=$(ARCH)-$(FORMAT)-objcopy
LD=$(ARCH)-$(FORMAT)-ld
OBJDUMP=$(ARCH)-$(FORMAT)-objdump

QEMU=qemu-system-$(ARCH)

C_SRCS=$(shell find . -name '*.c')
S_SRCS=$(shell find . -name '*.S')
OBJS=$(C_SRCS:.c=_c.o) $(S_SRCS:.S=_S.o) fonts/font.o
TEST_OBJS=$(C_SRCS:.c=_c_t.o) $(S_SRCS:.S=_S_t.o) fonts/font.o

BUILD_NUMBER_FILE=config/build-number.conf
BUILD_NUMBER_FLAG=-D__BUILD_NUMBER=$$(cat $(BUILD_NUMBER_FILE))

CFLAGS=-Wall -Werror -O2 -ffreestanding -nostdlib -nostartfiles -g -c -Isrc/ $(BUILD_NUMBER_FLAG) -D__DEVICE_TARGET=$(DEVICE_TARGET)
QEMU_FLAGS=-serial stdio
QEMU_TEST_FLAGS=-chardev stdio,id=char0,logfile=qemu_serial.log,signal=off \
	-serial chardev:char0 -display none


.PHONY: all run clean flash test doc

all: kernel8.img

$(BUILD_NUMBER_FILE): $(OBJS)
	@if ! test -f $(BUILD_NUMBER_FILE); then echo 0 > $(BUILD_NUMBER_FILE); fi
	@echo $$(($$(cat $(BUILD_NUMBER_FILE)) + 1)) > $(BUILD_NUMBER_FILE)

%_c.o: %.c
	$(GCC) $(CFLAGS) -c $< -o $@

%_S.o: %.S
	$(GCC) $(CFLAGS) -c $< -o $@

%_c_t.o: %.c
	$(GCC) $(CFLAGS) -DTEST_BUILD $< -o $@

%_S_t.o: %.S
	$(GCC) $(CFLAGS) -DTEST_BUILD $< -o $@

fonts/font.o: fonts/font.psf
	$(LD) -r -b binary -o $@ $<

neutrino.elf: $(BUILD_NUMBER_FILE) $(OBJS) src/arch/$(ARCH)/linker.ld
	$(LD) -nostdlib -nostartfiles $(OBJS) -T src/arch/$(ARCH)/linker.ld -o $@

kernel8.img: neutrino.elf
	$(OBJCOPY) -O binary $< $@
	
run: kernel8.img
	$(QEMU) -M raspi3 -kernel kernel8.img $(QEMU_FLAGS)

neutrino_test.elf: $(TEST_OBJS) src/arch/$(ARCH)/linker.ld
	$(LD) -nostdlib -nostartfiles $(TEST_OBJS) -T src/arch/$(ARCH)/linker.ld -o $@

kernel8_test.img: neutrino_test.elf
	$(OBJCOPY) -O binary $< $@

test: kernel8_test.img
	$(QEMU) -M raspi3 -kernel $< $(QEMU_TEST_FLAGS)

clean:
	rm kernel8.img kernel8_test.img neutrino.elf neutrino_test.elf qemu_serial.log $(OBJS) $(TEST_OBJS) >/dev/null 2>/dev/null || true

objdump:
	$(OBJDUMP) -C -g -S neutrino.elf

flash: kernel8.img
	cp ./kernel8.img /media/$$USER/BOOT/

doc:
	doxygen
