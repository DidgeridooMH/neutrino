<div align="center">
<img src="https://gitlab.com/DidgeridooMH/neutrino/raw/d54283dcef556baeeba0d65c6f46e60ffbd0eb17/assets/neutrino_logo_text.png?inline=false" height=120>
</div>

## Description

Neutrino is a small OS project targetting cross platform support although first
supporting the Raspberry Pi 3b. It aims to be a monolithic kernel in design, but
pass alot of high level support off to services to allow for customization.

# Disclaimer

This code is still early stage and as such contains code from various repos.
The sections from those repos are temporary and will be replaced with more
customized and robust implementations. However, I feel it would be a crime to
not mention them here first.

- https://github.com/bztsrc/raspi3-tutorial
- https://github.com/jsandler18/raspi-kernel
- https://github.com/rust-embedded/rust-raspi3-OS-tutorials

# Requirements

The requirements for building Neutrino vary depending on which hardware you are
targetting. However for simplicity it will be assumed you are targetting the
Raspberry Pi 3 (this can be emulated if you do not have a physical one). If you
want to only test Neutrino headless, it is recommended to use the
`docker/Dockerfile` to create a container and run Qemu from it.

## Setting up build environment variables.

| Variable | Value               | Description                                                                                           |
| -------- | ------------------- | ----------------------------------------------------------------------------------------------------- |
| `PREFIX` | `/opt/cross`        | The location to install software. This can be customized to whatever.                                 |
| `TARGET` | `aarch64-elf`       | Target architecture, change this to whatever hardware you are targetting.                             |
| `PATH`   | `$PREFIX/bin:$PATH` | Fixes the path to include new software. It is recommended to set this in`.bashrc` or your equivalent. |

## Prerequisites

On Ubuntu 18.04:

```sh
apt update
apt install -y build-essential bison flex libgmp3-dev libmpc-dev libmpfr-dev \
    texinfo python pkg-config git libglib2.0-dev libfdt-dev libpixman-1-dev \
    zlib1g-dev wget
```

## Build BinUtils

Neutrino has been built and tested on BinUtils version 2.31. The set of commands
to download this version and build can be found below. If you run into problems,
refer to the `docker/Dockerfile` for specific instructions.

```sh
wget "https://ftp.gnu.org/gnu/binutils/binutils-2.31.tar.gz"
tar -xvf binutils-2.31.tar.gz
mkdir build-binutils
cd build-binutils
../binutils-2.31/configure --target=${TARGET} --prefix="${PREFIX}"
make
make install
```

## Build GCC

Neutrino has been built and tested on GCC version 9.2.0. Building a GCC from
is a simple yet time consuming task. The set of commands to download this
version and build can be found below. If you run into problems, refer to the
`docker/Dockerfile` for specific instructions.

```sh
wget "https://ftp.gnu.org/gnu/gcc/gcc-8.2.0/gcc-8.2.0.tar.gz"
tar -xvf gcc-8.2.0.tar.gz
mkdir build-gcc
cd build-gcc
../gcc-8.2.0/configure --target=${TARGET} --prefix="${PREFIX}" --disable-nls --enable-languages=c --without-headers
make all-gcc
make all-target-libgcc
make install-gcc
make install-target-libgcc
rm -r ../gcc-8.2.0
```

## Build Qemu (Optional)

If you want to test Neutrino virtually, it is recommended that you use Qemu 4.2+
as this has the most support for the Raspberry Pi. The set of commands to
download this version and build can be found below. If you run into problems,
refer to the `docker/Dockerfile` for specific instructions.

```sh
wget https://download.qemu.org/qemu-4.2.0.tar.xz
tar xvJf qemu-4.2.0.tar.xz
cd qemu-4.2.0
./configure --target-list=aarch64-softmmu --enable-modules --enable-tcg-interpreter --enable-debug-tcg
make
make install
cd ..
rm -r qemu-4.2.0
```

# Running Neutrino (Emulator)

The makefile should automate this process as long as the build tools and Qemu
are in your path. The OS should start outputting serial to the console and wait
for a newline in stdin before beginning.

```sh
make
make run
```

# Running on Real Hardware

Instruction coming soon...
