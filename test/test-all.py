#!/usr/bin/env python3

import logging
import threading
import time
from subprocess import Popen, PIPE

# Increase this if tests take longer.
TESTING_TIME_SECS = 5


def start_test():
    logging.info("Test    : starting test process")
    global process
    process = Popen(['make', 'test'], stdin=None, stdout=None, stderr=None)


def check_results():
    tests_passed = 0
    total_tests = 0
    with open("qemu_serial.log", "r") as fp:
        line = fp.readline()
        while line:
            is_test = line.find("TEST") >= 0
            if is_test and line.find("PASSED") >= 0:
                tests_passed += 1
            if is_test:
                total_tests += 1
            line = fp.readline()
    return (tests_passed, total_tests)


if __name__ == "__main__":
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S")

    test_thread = threading.Thread(target=start_test)
    logging.info("Main    : starting test thread")
    test_thread.start()
    logging.info("Main    : waiting for test thread to finish")
    time.sleep(TESTING_TIME_SECS)
    logging.info("Main    : closing test thread")
    process.terminate()
    tests_passed, total_tests = check_results()
    logging.info("Main    : " + str(tests_passed) + "/" +
                 str(total_tests) + " tests succeeded")
    exit(total_tests - tests_passed)
