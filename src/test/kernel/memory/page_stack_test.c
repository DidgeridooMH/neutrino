#include "kernel/memory/page_stack.h"
#include "kernel/syscall/printf.h"
#include "page_stack_test.h"

static void page_stack_test_nfn() {
    void *first_page = page_stack_allocate();
    page_stack_deallocate(first_page);
    void *second_page = page_stack_allocate();
    printf("TEST : Testing one page off one page on : ");
    if (first_page == second_page) {
        printf("PASSED (0x%x, 0x%x)\n", first_page, second_page);
    } else {
        printf("FAILED (0x%x, 0x%x)\n", first_page, second_page);
    }
}

void page_stack_test_all() {
    page_stack_initialize();
    page_stack_test_nfn();
}
