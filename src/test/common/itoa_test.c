#include "common/itoa.h"
#include "../assert.h"
#include "common/string.h"
#include "kernel/syscall/printf.h"

#define ITOA_DEFINE_TEST(x, base) itoa_test_##x##base();

#define ITOA_TEST_CASE(x, expected, base)                                 \
    static void itoa_test_##x##base() {                                   \
        char buf[16];                                                     \
        const char *ref = expected;                                       \
        itoa(buf, x, base);                                               \
        printf("TEST : Testing itoa with %d input and base : ", x, base); \
        assert_equal(buf, ref);                                           \
    }

ITOA_TEST_CASE(0, "0", 10)
ITOA_TEST_CASE(1234, "1234", 10)
ITOA_TEST_CASE(0xDEADBEEF, "deadbeef", 16)

void itoa_test_all() {
    ITOA_DEFINE_TEST(0, 10);
    ITOA_DEFINE_TEST(1234, 10);
    ITOA_DEFINE_TEST(0xDEADBEEF, 16);
}