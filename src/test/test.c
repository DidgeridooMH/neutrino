#include "../bsp/rpi/uart.h"
#include "common/itoa_test.h"
#include "kernel/memory/memory_test.h"

#ifdef TEST_BUILD
void kmain() {
    uart_initialize();
    uart_puts(
        "TEST : Testing Uart configuration at 48mhz. This will not have an "
        "explicity fail. : PASSED\n");

    itoa_test_all();
    memory_test_all();
}
#endif
