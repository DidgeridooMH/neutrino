#ifndef _NEUTRINO_TEST_ASSERT_H_
#define _NEUTRINO_TEST_ASSERT_H_

void assert_equal(const char *test, const char *ref);

#endif
