#include "assert.h"
#include "common/string.h"
#include "kernel/syscall/printf.h"

#define TEST_PASS_REPORT(test, ref) \
    print("PASSED (");              \
    print(test);                    \
    print(",");                     \
    print(ref);                     \
    print(")\n");

#define TEST_FAIL_REPORT(test, ref) \
    print("FAILED (");              \
    print(test);                    \
    print(",");                     \
    print(ref);                     \
    print(")\n");

void assert_equal(const char *test, const char *ref) {
    if (strcmp(test, ref) == 0) {
        TEST_PASS_REPORT(test, ref);
    } else {
        TEST_FAIL_REPORT(test, ref);
    }
}