/**
 * @file device_list.c
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief A naive implementation of a device driver list.
 *
 * This is a rudementary implementation of a device list. It essentially is a
 * linked list of devices that are installed on the system. This should later
 * be replaced with a device tree that better groups devices together.
 */

#include <stddef.h>

#include "device_list.h"
#include "kernel/panic.h"
#include "kernel/syscall/alloc.h"
#include "kernel/syscall/printf.h"

/**
 * Device list and starting descriptor.
 */
static uint64_t device_pool = 1;
static DeviceDescriptor root = {"root device", 0, NULL, DI_ROOT, NULL};

/**
 * @brief Adds a descriptor to the device list.
 * @param descriptor A device descriptor to add.
 */
static void add_device_to_list(DeviceDescriptor *descriptor) {
    DeviceDescriptor *head = &root;
    while (head->next != NULL) {
        head = head->next;
    }
    head->next = descriptor;
}

/**
 * @brief Adds a device to the device list.
 * @param device_name Name of the device.
 * @param device_driver A reference to the device's driver structure.
 * @param type The type identifier for the device.
 * @returns The index of the device in the list.
 */
uint64_t add_device_to_system(const char *device_name, void *device_driver,
                              DriverType type) {
    if (device_pool == 0) {
        panic("Unable to assign new device id.");
    }

    DeviceDescriptor *new_desc = kmalloc(sizeof(DeviceDescriptor));
    new_desc->device_name = device_name;
    new_desc->device_id = device_pool;
    new_desc->device_driver = device_driver;
    new_desc->type = type;
    new_desc->next = NULL;

    add_device_to_list(new_desc);

    return device_pool++;
}

/**
 * @brief Removes a device from the list.
 * @param id The id of the device to removed from the list.
 * @returns The descriptor of the device that was removed or NULL on fail.
 */
static DeviceDescriptor *remove_device_from_list(uint64_t id) {
    DeviceDescriptor *head = &root;
    while (head->next != NULL && head->next->device_id != id) {
        head = head->next;
    }

    if (head->next == NULL) {
        return NULL;
    }

    DeviceDescriptor *device_to_remove = head->next;
    head->next = head->next->next;

    return device_to_remove;
}

/**
 * @brief Removes the device from the system.
 * @param id Index of the device to remove.
 * @returns Whether the device was successfully removed.
 */
int remove_device_from_system(uint64_t id) {
    if (id == 0) {
        printf("Removal error: Attempted to remove root device\n");
        return -1;
    }

    DeviceDescriptor *device = remove_device_from_list(id);
    if (device == NULL) {
        printf("Removal error: Device %u not found.\n", id);
        return -1;
    }

    kfree(device);

    return 0;
}

/**
 * @brief Retrieves the first device in the list of a specific type.
 * @param type Type of device to get.
 * @returns The driver interface for the found device or NULL if none is found.
 */
void *get_default_driver(DriverType type) {
    DeviceDescriptor *head = &root;

    while (head != NULL) {
        if (head->device_id == type) {
            return head->device_driver;
        }
        head = head->next;
    }

    return NULL;
}
