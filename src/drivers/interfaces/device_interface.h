/**
 * @file device_interface.h
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief List of possible interface types that devices can register as.
 *
 * List of possible interface types that devices can register as.
 */

#ifndef _NEUTRINO_DRIVERS_INTERFACES_DEVICE_INTERFACE_H_
#define _NEUTRINO_DRIVERS_INTERFACES_DEVICE_INTERFACE_H_

typedef enum { DI_ROOT, DI_GENERIC_DISPLAY } DriverType;

#endif
