/**
 * @file generic_display.h
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief A generic interface for writing to a graphics display.
 *
 * This file is meant to work as an interface for writing to a graphic display.
 * It should be implemented as a early stage graphics driver.
 */

#ifndef _NEUTRINO_DRIVERS_INTERFACE_GENERIC_DISPLAY_H_
#define _NEUTRINO_DRIVERS_INTERFACE_GENERIC_DISPLAY_H_

#include <stddef.h>
#include <stdint.h>

typedef enum {
    PF_RGBA,
    PF_ARGB,
    PF_RGB,
} PixelFormat;

struct GenericDisplay;

typedef void (*fp_draw_pixel)(size_t x, size_t y, uint32_t color,
                              struct GenericDisplay *driver);

struct GenericDisplay {
    PixelFormat format;
    size_t width;
    size_t height;
    void *device_info;
    fp_draw_pixel draw_pixel;
};
typedef struct GenericDisplay GenericDisplay;

#endif
