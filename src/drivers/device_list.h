/**
 * @file device_list.h
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief A naive implementation of a device driver list.
 *
 * This is a rudementary implementation of a device list. It essentially is a
 * linked list of devices that are installed on the system. This should later
 * be replaced with a device tree that better groups devices together.
 */

#ifndef _NEUTRINO_DRIVERS_INTERFACES_DEVICE_LIST_H_
#define _NEUTRINO_DRIVERS_INTERFACES_DEVICE_LIST_H_

#include <stdint.h>

#include "interfaces/device_interface.h"

struct DeviceDescriptor {
    const char *device_name;
    uint64_t device_id;
    void *device_driver;
    DriverType type;
    struct DeviceDescriptor *next;
};
typedef struct DeviceDescriptor DeviceDescriptor;

/**
 * @brief Adds a device to the device list.
 * @param device_name Name of the device.
 * @param device_driver A reference to the device's driver structure.
 * @param type The type identifier for the device.
 * @returns The index of the device in the list.
 */
uint64_t add_device_to_system(const char *device_name, void *device_driver,
                              DriverType type);

/**
 * @brief Removes the device from the system.
 * @param id Index of the device to remove.
 * @returns Whether the device was successfully removed.
 */
int remove_device_from_system(uint64_t id);

/**
 * @brief Retrieves the first device in the list of a specific type.
 * @param type Type of device to get.
 * @returns The driver interface for the found device or NULL if none is found.
 */
void *get_default_driver(DriverType type);

#endif
