/**
 * @file atag.h
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief This file provides information that is defined in the Atags by the
 *        bootloader.
 *
 * This file provides information that is defined in the Atags by the
 * bootloader.
 */

#ifndef _NEUTRINO_BSP_RPI_ATAG_H_
#define _NEUTRINO_BSP_RPI_ATAG_H_

#include <stddef.h>
#include <stdint.h>

/// Default address that Atags are stored in at boot.
#define ATAG_ADDRESS 0x100u

/// These are the various types of Atags.
#define ATAG_NULL 0x00000000
#define ATAG_CORE 0x54410001
#define ATAG_MEMORY 0x54410002

typedef struct {
    uint32_t size;
    uint32_t tag;
} __attribute__((packed)) Atag;

typedef struct {
    Atag tag;
    uint32_t memory_size;
    uint32_t memory_start;
} __attribute__((packed)) MemoryAtag;

/**
 * @brief Gets the amount of available memory in bytes.
 * @returns Amount of available memory in bytes.
 */
size_t get_memory_size();

#endif
