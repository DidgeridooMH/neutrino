/**
 * @file peripheral.h
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief Defines information pertaining to the peripheral package of the RPi.
 *
 * Defines information pertaining to the peripheral package of the RPi.
 */

#ifndef _NEUTRINO_BSP_RPI_PERIPHERAL_H_
#define _NEUTRINO_BSP_RPI_PERIPHERAL_H_

#define PERIPHERAL_BASE 0x3F000000

/**
 * @brief When the hardware sends an address to the CPU it is in a raw address
 *        form. This needs to be translated into a memory address mapped to the
 *        CPU.
 * @param addr Physical address to convert.
 */
#define phys_to_virt(addr) (addr & ~0xC0000000)

#endif
