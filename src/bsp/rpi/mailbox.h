/**
 * @file mailbox.h
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief This file provides an interface for utilizing the mailbox system that
 *        communicates with the VideoCore.
 *
 * This file provides an interface for utilizing the mailbox system that
 * communicates with the VideoCore. Usually usage involves filling the mailbox,
 * sending to the VideoCore's desired channel, then reading until a response
 * comes. For this reason, this should be used synchonously only.
 *
 * TODO: Added locks to ensure only one process uses the mailbox at a time.
 * TODO: Currently the process can lock up reading doesn't find a response.
 * TODO: Implement interrupts instead of synchonous reading/writing?
 */

#ifndef _NEUTRINO_BSP_RPI_MAILBOX_H_
#define _NEUTRINO_BSP_RPI_MAILBOX_H_

#include <stdint.h>
#include "peripheral.h"

#define MAILBOX_SIZE 32

/// Mailbox MMIO registers.
#define MAILBOX_BASE (PERIPHERAL_BASE + 0xB880)
#define MAILBOX_READ (MAILBOX_BASE)
#define MAILBOX_STATUS (MAILBOX_BASE + 0x18)
#define MAILBOX_WRITE (MAILBOX_BASE + 0x20)

/// Mailbox peripheral bit definitions.
#define MAILBOX_EMPTY (1u << 30u)
#define MAILBOX_FULL (1u << 31u)

/// These are the various tags that the VideoCore accepts.
#define CLOCK_RATE_SET_RATE 0x00038002
#define CLOCK_RATE_GET_MAX_RATE 0x00030004
#define FRAMEBUFFER_ALLOCATE_BUFFER 0x00040001
#define FRAMEBUFFER_RELEASE_BUFFER 0x00048001
#define FRAMEBUFFER_GET_PHYS_DIM 0x00040003
#define FRAMEBUFFER_SET_PHYS_DIM 0x00048003
#define FRAMEBUFFER_GET_VIRT_DIM 0x00040004
#define FRAMEBUFFER_SET_VIRT_DIM 0x00048004
#define FRAMEBUFFER_GET_BITS_PER_PIXEL 0x00040005
#define FRAMEBUFFER_SET_BITS_PER_PIXEL 0x00048005
#define FRAMEBUFFER_SET_PIXEL_FORMAT 0x00048006
#define FRAMEBUFFER_GET_BYTES_PER_ROW 0x00040008

/// These are the various channels that the VideoCore supports.
#define PROPERTY_CHANNEL 8

/// Reference to the global mailbox memory area.
extern uint32_t g_mailbox[MAILBOX_SIZE];

/**
 * @brief Sends the Mailbox message to the VideoCore.
 * @param channel Channel the message should target.
 */
void mailbox_send(uint8_t channel);

/**
 * @brief Reads a response from the VideoCore on a specific channel.
 * @param channel Channel to target when reading.
 * @returns The response code from the VideoCore.
 */
uint32_t mailbox_read(uint8_t channel);

#endif
