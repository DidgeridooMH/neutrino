/**
 * @file uart.c
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief A driver for interfacing with the UART0 hardware on Raspberry Pi (3b).
 *
 * A driver for interfacing with the UART0 hardware on Raspberry Pi (3b). This
 * code is heavily based on jsandler18's kernel tutorial code.
 *
 * https://github.com/jsandler18/raspi-kernel
 *
 * TODO: Customize this driver and make its code my own.
 */

#include "uart.h"
#include "../../common/mmio.h"
#include "../../common/utils.h"

/**
 * @brief Initializes the UART0 hardware.
 */
void uart_initialize() {
    // Disable all UART0 Hardware
    mmio_write(UART0_CR, 0u);

    uint32_t selector = mmio_read(GPFSEL1);
    selector &= ~((7u << 12u) | (7u << 15u));
    selector |= (4u << 12u) | (4u << 15u);
    mmio_write(GPFSEL1, selector);

    // Prepare GPIO pins are going to be disabled.
    mmio_write(GPPUD, 0u);
    wait_for_cycles(GPIO_COOLDOWN_LENGTH);

    // Select pins 14 and 15.
    mmio_write(GPPUDCLK0, (1u << 14u) | (1u << 15u));
    wait_for_cycles(GPIO_COOLDOWN_LENGTH);

    // Apply the settings.
    mmio_write(GPPUDCLK0, 0u);

    // Clear all the pending interrupts from UART hardware.
    mmio_write(UART0_ICR, 0x7FFu);

    // Set the baud rate. We set ours to 48 Mhz.
    mmio_write(UART0_IBRD, 26u);
    mmio_write(UART0_FBRD, 3u);

    /*
     * Set line control register to use an 8 item FIFO for uart and use 8 bit
     * words.
     */
    mmio_write(UART0_LCRH, 0b11u << 5u);

    // Disable all Uart interrupts.
    mmio_write(UART0_IMSC, 0x7F2u);

    // Enable Uart hardware and enable Rx/Tx
    mmio_write(UART0_CR, 0x301u);
}

/**
 * @brief Retrieves a character from the UART0 serial port.
 * @returns Character read from the serial port.
 */
char uart_getc() {
    // Wait for a character to be available in the queue.
    while (mmio_read(UART0_FR) & (1u << 4u)) {
        asm volatile("nop");
    }

    char c = (char)mmio_read(UART0_DR);

    return c == '\r' ? '\n' : c;
}

/**
 * @brief Writes a character to the UART0 serial port.
 * @param c Character to write to serial port.
 */
static void uart_putc(char c) {
    // Convert line endings.
    if (c == '\n') {
        uart_putc('\r');
    }

    // Wait for a space in the queue.
    while (mmio_read(UART0_FR) & (1u << 5u)) {
        asm volatile("nop");
    }

    mmio_write(UART0_DR, (uint32_t)c);
}

/**
 * @brief Writes a string to the UART0 serial port.
 * @param str String to write to serial port.
 */
void uart_puts(const char *str) {
    int i = 0;
    while (str[i] != '\0') {
        uart_putc(str[i]);
        ++i;
    }
}
