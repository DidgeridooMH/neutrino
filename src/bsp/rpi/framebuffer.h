/**
 * @file framebuffer.h
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief An interface for allocating and deallocating a framebuffer for the
 *        display.
 *
 * An interface for allocating and deallocating a framebuffer for the
 * display.
 */

#ifndef _NEUTRINO_BSP_RPI_FRAMEBUFFER_H_
#define _NEUTRINO_BSP_RPI_FRAMEBUFFER_H_

#include <stddef.h>
#include <stdint.h>

/// Number of bits per pixel. ARGB has 8 per channel.
#define FB_DEFAULT_DEPTH 32

typedef struct {
    uint32_t *buffer;
    size_t size;
    uint32_t width;
    uint32_t height;
    uint32_t depth;
} FrameBuffer;

/**
 * @brief Requests a framebuffer from the VideoCore.
 * @returns A framebuffer structure created by the VideoCore.
 */
FrameBuffer *request_framebuffer();

/**
 * @brief Release a framebuffer back to the VideoCore.
 * @param framebuffer The framebuffer structure to release.
 */
void release_framebuffer(FrameBuffer *framebuffer);

#endif
