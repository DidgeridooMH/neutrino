/**
 * @file mailbox.c
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief This file provides an interface for utilizing the mailbox system that
 *        communicates with the VideoCore.
 *
 * This file provides an interface for utilizing the mailbox system that
 * communicates with the VideoCore. Usually usage involves filling the mailbox,
 * sending to the VideoCore's desired channel, then reading until a response
 * comes. For this reason, this should be used synchonously only.
 *
 * TODO: Added locks to ensure only one process uses the mailbox at a time.
 * TODO: Currently the process can lock up reading doesn't find a response.
 * TODO: Implement interrupts instead of synchonous reading/writing?
 */

#include "mailbox.h"
#include "common/mmio.h"
#include "kernel/syscall/printf.h"

/**
 * The global mailbox memory area. The VideoCore requires this to be 16-byte
 * aligned.
 */
uint32_t g_mailbox[MAILBOX_SIZE] __attribute__((aligned(16)));

/**
 * @brief Sends the Mailbox message to the VideoCore.
 * @param channel Channel the message should target.
 */
void mailbox_send(uint8_t channel) {
    uint32_t status = MAILBOX_FULL;

    while ((status & MAILBOX_FULL) > 0) {
        status = mmio_read(MAILBOX_STATUS);
    }

    mmio_write(MAILBOX_WRITE,
               (uint32_t)(uint64_t)g_mailbox | ((uint32_t)channel & 0xF));
}

/**
 * @brief Reads a response from the VideoCore on a specific channel.
 * @param channel Channel to target when reading.
 * @returns The response code from the VideoCore.
 */
uint32_t mailbox_read(uint8_t channel) {
    uint32_t response = 0;
    uint32_t res_status = 0;

    do {
        do {
            res_status = mmio_read(MAILBOX_STATUS);
        } while ((res_status & MAILBOX_EMPTY) > 0);
        response = mmio_read(MAILBOX_READ);
    } while ((response & 0xF) != (uint32_t)channel);

    return response;
}
