/**
 * @file atag.c
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief This file provides information that is defined in the Atags by the
 *        bootloader.
 *
 * This file provides information that is defined in the Atags by the
 * bootloader.
 */

#include "atag.h"

/**
 * @brief Gets the amount of available memory in bytes.
 * @returns Amount of available memory in bytes.
 */
size_t get_memory_size() {
    Atag *list = (Atag *)ATAG_ADDRESS;

    while (list->tag != ATAG_NULL) {
        if (list->tag == ATAG_MEMORY) {
            return (size_t)((MemoryAtag *)list)->memory_size;
        }
        list = (Atag *)((void *)list + list->size * sizeof(uint32_t));
    }

    return 0UL;
}