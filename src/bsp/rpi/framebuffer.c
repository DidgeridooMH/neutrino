/**
 * @file framebuffer.h
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief An interface for allocating and deallocating a framebuffer for the
 *        display.
 *
 * An interface for allocating and deallocating a framebuffer for the
 * display.
 */

#include "framebuffer.h"
#include "common/string.h"
#include "kernel/panic.h"
#include "kernel/syscall/alloc.h"
#include "kernel/syscall/printf.h"
#include "mailbox.h"

/**
 * @brief Retreives the physical dimensions of the display in pixels.
 * @param width Reference to a location to store the width in pixels.
 * @param height Reference to a location to store the height in pixels.
 */
static void get_physical_dimensions(uint32_t *width, uint32_t *height) {
    uint32_t mailbox[] = {32, 0, FRAMEBUFFER_GET_PHYS_DIM, 8, 0, 0, 0, 0};
    memcpy(g_mailbox, mailbox, sizeof(mailbox));

    mailbox_send(PROPERTY_CHANNEL);
    mailbox_read(PROPERTY_CHANNEL);

    if (g_mailbox[1] != 0x80000000) {
        printf("Error retreiving display settings...Code: 0x%x\n",
               g_mailbox[1]);
        panic("DISPLAY BUFFER COULD NOT BE MADE!!!");
    }

    *width = g_mailbox[5];
    *height = g_mailbox[6];

    printf("Display is %ux%u\n", *width, *height);
}

/**
 * @brief Allocates a framebuffer from the VideoCore.
 * @param buffer A location to store the returned framebuffer.
 * @param width Desired width of the framebuffer.
 * @param height Desired height of the framebuffer.
 */
static void allocate_framebuffer(FrameBuffer *buffer, uint32_t width,
                                 uint32_t height) {
    uint32_t mailbox[] = {0,
                          0,
                          FRAMEBUFFER_SET_PHYS_DIM,
                          8,
                          8,
                          width,
                          height,
                          FRAMEBUFFER_SET_VIRT_DIM,
                          8,
                          8,
                          width,
                          height,
                          FRAMEBUFFER_SET_BITS_PER_PIXEL,
                          4,
                          4,
                          FB_DEFAULT_DEPTH,
                          FRAMEBUFFER_SET_PIXEL_FORMAT,
                          4,
                          4,
                          1,
                          FRAMEBUFFER_ALLOCATE_BUFFER,
                          8,
                          4,
                          16,
                          0,
                          0};
    mailbox[0] = sizeof(mailbox);
    memcpy(g_mailbox, mailbox, sizeof(mailbox));

    mailbox_send(PROPERTY_CHANNEL);
    mailbox_read(PROPERTY_CHANNEL);

    if (g_mailbox[1] != 0x80000000) {
        printf("Error retreiving display settings....Code: 0x%x\n",
               g_mailbox[1]);
        panic("DISPLAY BUFFER COULD NOT BE MADE!!!");
    }

    if (g_mailbox[22] != 0x80000008) {
        printf("Invalid response for display setting...Code: 0x%x\n",
               g_mailbox[22]);
        panic("DISPLAY BUFFER COULD NOT BE MADE!!!");
    }

    buffer->buffer = (uint32_t *)(uint64_t)phys_to_virt(g_mailbox[23]);
    buffer->size = g_mailbox[24];
    buffer->depth = FB_DEFAULT_DEPTH;
    buffer->width = width;
    buffer->height = height;
}

/**
 * @brief Requests a framebuffer from the VideoCore.
 * @returns A framebuffer structure created by the VideoCore.
 */
FrameBuffer *request_framebuffer() {
    FrameBuffer *buffer = kmalloc(sizeof(FrameBuffer));
    uint32_t width = 0u;
    uint32_t height = 0u;

    get_physical_dimensions(&width, &height);

    allocate_framebuffer(buffer, width, height);

    return buffer;
}

/**
 * @brief Release a framebuffer back to the VideoCore.
 * @param framebuffer The framebuffer structure to release.
 */
void release_framebuffer(FrameBuffer *framebuffer) {
    // TODO: Send a message to Video Core to release buffer.
    kfree(framebuffer);
}
