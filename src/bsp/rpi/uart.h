/**
 * @file uart.h
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief A driver for interfacing with the UART0 hardware on Raspberry Pi (3b).
 *
 * A driver for interfacing with the UART0 hardware on Raspberry Pi (3b). This
 * code is heavily based on jsandler18's kernel tutorial code.
 *
 * https://github.com/jsandler18/raspi-kernel
 */

#ifndef _NEUTRINO_BSP_RPI_UART_H_
#define _NEUTRINO_BSP_RPI_UART_H_

#include "gpio.h"

#define UART0_BASE (GPIO_BASE + 0x1000)
#define UART0_DR UART0_BASE
#define UART0_FR (UART0_BASE + 0x18)
#define UART0_IBRD (UART0_BASE + 0x24)
#define UART0_FBRD (UART0_BASE + 0x28)
#define UART0_LCRH (UART0_BASE + 0x2C)
#define UART0_CR (UART0_BASE + 0x30)
#define UART0_IMSC (UART0_BASE + 0x38)
#define UART0_ICR (UART0_BASE + 0x44)

/**
 * @brief Initializes the UART0 hardware.
 */
void uart_initialize();

/**
 * @brief Retrieves a character from the UART0 serial port.
 * @returns Character read from the serial port.
 */
char uart_getc();

/**
 * @brief Writes a string to the UART0 serial port.
 * @param str String to write to serial port.
 */
void uart_puts(const char *str);

#endif
