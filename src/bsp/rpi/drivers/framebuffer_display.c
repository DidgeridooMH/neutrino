/**
 * @file framebuffer_display.c
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief Handles the creation and destruction of the framebuffer display
 *        driver.
 *
 * Handles the creation and destruction of the framebuffer display
 * driver.
 */

#include "framebuffer_display.h"
#include "bsp/rpi/framebuffer.h"
#include "kernel/syscall/alloc.h"

/**
 * @brief Draws a single pixel to the framebuffer.
 * @param x X position to draw pixel.
 * @param y Y position to draw pixel.
 * @param color Color code to draw to pixel in RGBA format.
 */
static void draw_framebuffer_pixel(size_t x, size_t y, uint32_t color,
                                   GenericDisplay *driver) {
    if (x < driver->width || y < driver->height) {
        FrameBuffer *framebuffer = (FrameBuffer *)driver->device_info;
        framebuffer->buffer[x + y * framebuffer->width] = color;
    }
}

/**
 * @brief Creates a framebuffer driver.
 */
GenericDisplay *create_framebuffer_context() {
    FrameBuffer *framebuffer = request_framebuffer();

    GenericDisplay *display_driver =
        (GenericDisplay *)kmalloc(sizeof(GenericDisplay));
    display_driver->format = PF_RGBA;
    display_driver->width = (size_t)framebuffer->width;
    display_driver->height = (size_t)framebuffer->height;
    display_driver->device_info = (void *)framebuffer;
    display_driver->draw_pixel = draw_framebuffer_pixel;

    return display_driver;
}

/**
 * @brief Safely destroys a framebuffer driver.
 */
void destroy_framebuffer_context(GenericDisplay *context) {
    release_framebuffer((FrameBuffer *)context->device_info);
    kfree(context);
}
