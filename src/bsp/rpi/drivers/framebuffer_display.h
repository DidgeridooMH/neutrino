/**
 * @file framebuffer_display.h
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief Handles the creation and destruction of the framebuffer display
 *        driver.
 *
 * Handles the creation and destruction of the framebuffer display
 * driver.
 *
 * TODO: Add additional driver features to increase performance.
 */

#ifndef _NEUTRINO_BSP_RPI_DRIVERS_FRAMEBUFFER_DISPLAY_H_
#define _NEUTRINO_BSP_RPI_DRIVERS_FRAMEBUFFER_DISPLAY_H_

#include "drivers/interfaces/generic_display.h"

/**
 * @brief Creates a framebuffer driver.
 */
GenericDisplay *create_framebuffer_context();

/**
 * @brief Safely destroys a framebuffer driver.
 */
void destroy_framebuffer_context(GenericDisplay *context);

#endif
