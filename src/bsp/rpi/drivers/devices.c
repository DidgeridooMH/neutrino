/**
 * @file devices.c
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief This file handles the registration of the RPi's devices to the system.
 *
 * This file handles the registration of the RPi's devices to the system. This
 * includes adding the devices to the device list and initializing them.
 */

#include "devices.h"
#include "drivers/device_list.h"
#include "framebuffer_display.h"

/**
 * @brief Generates device drivers and places their entries in the device tree.
 */
void generate_device_drivers() {
    GenericDisplay *display = create_framebuffer_context();
    add_device_to_system("HDMI GRAPHICS FRAMEBUFFER", display,
                         DI_GENERIC_DISPLAY);
}
