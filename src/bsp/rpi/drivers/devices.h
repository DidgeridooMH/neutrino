/**
 * @file devices.h
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief This file handles the registration of the RPi's devices to the system.
 *
 * This file handles the registration of the RPi's devices to the system. This
 * includes adding the devices to the device list and initializing them.
 */

#ifndef _NEUTRINO_BSP_RPI_DRIVERS_DEVICES_H_
#define _NEUTRINO_BSP_RPI_DRIVERS_DEVICES_H_

/**
 * @brief Generates device drivers and places their entries in the device tree.
 */
void generate_device_drivers();

#endif
