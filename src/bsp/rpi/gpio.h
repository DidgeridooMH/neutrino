/**
 * @file gpio.h
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief This file holds some essential GPIO values and registers.
 *
 * This file holds some essential GPIO values and registers.
 */

#ifndef _NEUTRINO_BSP_RPI_GPIO_H_
#define _NEUTRINO_BSP_RPI_GPIO_H_

#include "peripheral.h"

/// GPIO cooldown in cycles
#define GPIO_COOLDOWN_LENGTH 150

/// These are the various GPIO MMIO registers.
#define GPIO_BASE (PERIPHERAL_BASE + 0x200000)
#define GPFSEL1 (GPIO_BASE + 0x04)
#define GPPUD (GPIO_BASE + 0x94)
#define GPPUDCLK0 (GPIO_BASE + 0x98)

#endif
