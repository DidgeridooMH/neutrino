/**
 * @file utils.h
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief Miscellaneous utilities for the system.
 *
 * Miscellaneous utilities for the system such as spin waiting.
 */

#ifndef _NEUTRINO_COMMON_UTILS_H_
#define _NEUTRINO_COMMON_UTILS_H_

#include <stdint.h>

/**
 * @brief Holds the CPU for a specified amount of cycles before returning.
 * @param cycles Cycles to wait for.
 */
void wait_for_cycles(uint32_t cycles);

#endif
