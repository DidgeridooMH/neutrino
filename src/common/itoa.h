/**
 * @file itoa.h
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief A set of utilities for converting integer values into string format.
 *
 * A set of utilities for converting integer values into string format.
 */

#ifndef _NEUTRINO_COMMON_ITOA_H_
#define _NEUTRINO_COMMON_ITOA_H_

#include <stdint.h>

/**
 * @brief Converts an integer to a string.
 * @param buf Buffer to store string in.
 * @param n Integer to convert
 * @param base Numeric base to represent number in.
 */
void itoa(char *buf, uint32_t n, int base);

#endif
