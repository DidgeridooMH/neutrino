/**
 * @file utils.c
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief Miscellaneous utilities for the system.
 *
 * Miscellaneous utilities for the system such as spin waiting.
 */
#include "utils.h"

/**
 * @brief Holds the CPU for a specified amount of cycles before returning.
 * @param cycles Cycles to wait for.
 */
void inline wait_for_cycles(uint32_t cycles) {
    while (cycles--) {
        asm volatile("nop");
    }
}
