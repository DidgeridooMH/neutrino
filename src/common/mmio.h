/**
 * @file mmio.h
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief Utilities for writing to memory mapped io.
 *
 * Utilities for writing to memory mapped io. This is useful because mmio
 * operations are volatile and can sometimes be mishandled by a compiler.
 */

#ifndef _NEUTRINO_COMMON_MMIO_H_
#define _NEUTRINO_COMMON_MMIO_H_

#include <stdint.h>

/**
 * @brief Writes to a memory mapped address.
 * @param address Address to write to.
 * @param value Value to write.
 */
void mmio_write(uint32_t address, uint32_t value);

/**
 * @brief Reads from a memory mapped address.
 * @param address Address to read from.
 * @returns Value at the address specified.
 */
uint32_t mmio_read(uint32_t address);

#endif
