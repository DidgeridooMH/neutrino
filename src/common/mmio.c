/**
 * @file mmio.c
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief Utilities for writing to memory mapped io.
 *
 * Utilities for writing to memory mapped io. This is useful because mmio
 * operations are volatile and can sometimes be mishandled by a compiler.
 */

#include "mmio.h"

/**
 * @brief Writes to a memory mapped address.
 * @param address Address to write to.
 * @param value Value to write.
 */
inline void mmio_write(uint32_t address, uint32_t value) {
    *(volatile uint32_t*)((uint64_t)address) = value;
}

/**
 * @brief Reads from a memory mapped address.
 * @param address Address to read from.
 * @returns Value at the address specified.
 */
inline uint32_t mmio_read(uint32_t address) {
    return *(volatile uint32_t*)((uint64_t)address);
}
