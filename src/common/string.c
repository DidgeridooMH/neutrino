/**
 * @file string.c
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief A set of useful memory utilities.
 *
 * This file defines useful memory and string utilities.
 */

#include "string.h"

/**
 * @brief Copies memory from one location to another.
 * @param destination Starting address to write memory to.
 * @param source Starting address to read memory from.
 * @param num Number of bytes to copy.
 * @returns The destination address
 */
void *memcpy(void *destination, const void *source, size_t num) {
    char *dst = destination;
    const char *src = source;
    int i = 0;
    while (i < num) {
        dst[i] = src[i];
        i++;
    }
    return destination;
}

/**
 * @brief Calculates the length of a null-terminated string.
 * @param str String to calculate length of.
 * @returns Length of the string.
 */
size_t strlen(const char *str) {
    size_t count = 0;

    while (*(str++) != '\0') {
        ++count;
    }

    return count;
}

/**
 * @brief Compares two strings for equality.
 * @param str1 First string to compare.
 * @param str2 Second string to compare.
 * @returns Number of characters that is different in the strings or -1 if they
 *          are of different lengths.
 */
int strcmp(const char *str1, const char *str2) {
    int count = 0;

    if (strlen(str1) != strlen(str2)) {
        return -1;
    }

    while (*str1 != '\0') {
        if (*str1 != *str2) {
            ++count;
        }
        ++str1;
        ++str2;
    }

    return count;
}
