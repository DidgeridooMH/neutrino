/**
 * @file itoa.c
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief A set of utilities for converting integer values into string format.
 *
 * A set of utilities for converting integer values into string format.
 */

#include "itoa.h"

/**
 * @brief Converts an integer to a string.
 * @param buf Buffer to store string in.
 * @param n Integer to convert
 * @param base Numeric base to represent number in.
 */
void itoa(char *buf, uint32_t n, int base) {
    int ptr = 0;

    do {
        unsigned int currentNum = n % base;
        buf[ptr++] =
            (currentNum < 10) ? (currentNum + '0') : (currentNum - 10 + 'a');
    } while (n /= base);

    buf[ptr] = 0;

    int ptr2 = 0;
    --ptr;

    while (ptr2 < ptr) {
        int tmp = buf[ptr2];
        buf[ptr2] = buf[ptr];
        buf[ptr] = tmp;

        --ptr;
        ++ptr2;
    }
}
