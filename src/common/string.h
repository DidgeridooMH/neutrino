/**
 * @file string.h
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief A set of useful memory utilities.
 *
 * This file defines useful memory and string utilities.
 */

#ifndef _NEUTRINO_COMMON_STRING_H_
#define _NEUTRINO_COMMON_STRING_H_

#include <stddef.h>

/**
 * @brief Copies memory from one location to another.
 * @param destination Starting address to write memory to.
 * @param source Starting address to read memory from.
 * @param num Number of bytes to copy.
 * @returns The destination address
 */
void *memcpy(void *destination, const void *source, size_t num);

/**
 * @brief Calculates the length of a null-terminated string.
 * @param str String to calculate length of.
 * @returns Length of the string.
 */
size_t strlen(const char *str);

/**
 * @brief Compares two strings for equality.
 * @param str1 First string to compare.
 * @param str2 Second string to compare.
 * @returns Number of characters that is different in the strings or -1 if they
 *          are of different lengths.
 */
int strcmp(const char *str1, const char *str2);

#endif
