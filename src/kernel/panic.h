/**
 * @file panic.h
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief An error reporter for when the kernel can no longer continue.
 *
 * This component contains a set of kernel error routines. Its purpose is to
 * give final information on why the system had to halt.
 */

#ifndef _NEUTRINO_KERNEL_PANIC_H_
#define _NEUTRINO_KERNEL_PANIC_H_

/**
 * @brief Report an error that caused the kernel to stop.
 *
 * @param message A message to display before halting the system.
 */
void panic(const char *message);

#endif
