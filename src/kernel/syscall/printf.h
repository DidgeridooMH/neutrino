/**
 * @file printf.h
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief An interface for printing to the standard output streams.
 *
 * An interface for printing to the standard output streams.
 *
 * TODO: Add fprintf function.
 */

#ifndef _NEUTRINO_KERNEL_SYSCALL_PRINTF_H_
#define _NEUTRINO_KERNEL_SYSCALL_PRINTF_H_

/**
 * @brief Prints a string to the default output driver.
 * @param message Null-terminated string to print.
 */
void print(const char *message);

/**
 * @brief Parses and prints a formatted string to the default output driver.
 * @param format Null-terminated string describing the format of the message.
 * @param ... Variadic variable that contains the variables referenced in
 *            format.
 */
void printf(const char *format, ...);

#endif
