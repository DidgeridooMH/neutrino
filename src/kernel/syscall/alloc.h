/**
 * @file alloc.h
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief Allocation interface for dynamic kernel memory.
 *
 * Allocation interface for dynamic kernel memory. This implementation is
 * similar to what will be used for userland.
 */

#ifndef _NEUTRINO_KERNEL_SYSCALL_ALLOC_H_
#define _NEUTRINO_KERNEL_SYSCALL_ALLOC_H_

#include <stddef.h>

/// Heap size is 1 Mb for the kernel.
#define KERNEL_HEAP_SIZE (1 * 1024 * 1024)

/**
 * @brief Initializes the kernel heap structure.
 */
void kernel_heap_initialize();

/**
 * @brief Allocates a segment of memory for kernel use.
 * @param size Size of the segment to allocate.
 * @returns Starting address of the requested segment or NULL on failure.
 */
void *kmalloc(size_t size);

/**
 * @brief Releases a segment of memory back into the memory pool.
 * @param address Starting address of the segment to release.
 */
void kfree(void *address);

#endif
