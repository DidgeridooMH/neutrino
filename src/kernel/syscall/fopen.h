/**
 * @file fopen.h
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief An interface for opening and creating file descriptors.
 *
 * An interface for opening and creating file descriptors.
 */

#ifndef _NEUTRINO_KERNEL_SYSCALL_FOPEN_H_
#define _NEUTRINO_KERNEL_SYSCALL_FOPEN_H_

#include "kernel/stdio/file.h"

typedef enum {
    FM_WRITE_ONLY = 0,
    FM_READ_ONLY = 1,
    FM_READ_WRITE = 2
} FILE_MODE;

/**
 * @brief Creates an empty file desciptor with the specified mode.
 * @param mode Mode to open file in.
 * @returns An empty file descriptor.
 */
FILE *create_file(FILE_MODE mode);

#endif
