/**
 * @file printf.c
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief An interface for printing to the standard output streams.
 *
 * An interface for printing to the standard output streams.
 */
#include <stdarg.h>

#include "../../bsp/rpi/uart.h"
#include "../../common/itoa.h"
#include "common/string.h"
#include "kernel/stdio/file.h"
#include "printf.h"

/**
 * @brief Prints a string to the default output driver.
 * @param message Null-terminated string to print.
 */
void print(const char *message) {
    if (!stdout->opened) {
        uart_puts(message);
    } else {
        stdout->write_file((char *)message, strlen(message), stdout);
    }
}

/**
 * @brief Prints a character to the standard output driver.
 * @param c Character to print.
 */
static void print_c(char c) {
    char char_buf[2];
    char_buf[0] = c;
    char_buf[1] = '\0';
    print(char_buf);
}

/**
 * @brief Prints a signed integer to the standard output driver.
 * @param num Integer to print.
 */
static void print_i(int num) {
    char num_buf[16];
    if (num < 0) {
        print("-");
        num *= -1;
    }
    itoa(num_buf, num, 10);
    print(num_buf);
}

/**
 * @brief Prints a number in octal form to the standard output driver.
 * @param num Integer to print.
 */
static void print_o(uint32_t num) {
    char num_buf[16];
    itoa(num_buf, num, 8);
    print(num_buf);
}

/**
 * @brief Prints an unsigned integer to the standard output driver.
 * @param num Integer to print.
 */
static void print_u(uint32_t num) {
    char num_buf[16];
    itoa(num_buf, num, 10);
    print(num_buf);
}

/**
 * @brief Prints an integer in hex form to the standard output driver.
 * @param num Integer to print.
 */
static void print_x(int num) {
    char num_buf[32];
    itoa(num_buf, num, 16);
    print(num_buf);
}

/**
 * @brief Parses and prints a formatted string to the default output driver.
 * @param format Null-terminated string describing the format of the message.
 * @param ... Variadic variable that contains the variables referenced in
 *            format.
 */
void printf(const char *format, ...) {
    va_list args;
    va_start(args, format);
    int index = 0;
    while (format[index] != '\0') {
        if (format[index] == '%') {
            ++index;
            switch (format[index]) {
                case 'c':
                    print_c((char)va_arg(args, int));
                    break;
                case 'd':
                case 'i':
                    print_i(va_arg(args, int));
                    break;
                case 'o':
                    print_o(va_arg(args, int));
                    break;
                case 's':
                    // TODO: Change this to print. Huge security flaw.
                    printf(va_arg(args, char *));
                    break;
                case 'u':
                    print_u(va_arg(args, uint32_t));
                    break;
                case 'x':
                    print_x(va_arg(args, int));
                    break;
                case '%':
                    print_c('%');
                    break;
                default:
                    // TODO: Maybe throw an error?
                    break;
            }
        } else {
            print_c(format[index]);
        }
        ++index;
    }
}
