/**
 * @file alloc.c
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief Allocation interface for dynamic kernel memory.
 *
 * Allocation interface for dynamic kernel memory. This implementation is
 * similar to what will be used for userland.
 */

#include "alloc.h"
#include "../memory/heap.h"
#include "../memory/page_stack.h"
#include "printf.h"

/// Global kernel heap variable.
static HeapHeader *kernel_heap;

/**
 * @brief Initializes the kernel heap structure.
 */
void kernel_heap_initialize() {
    int num_of_kernel_pages = KERNEL_HEAP_SIZE / PAGE_SIZE;

    // Store first page as the heap starting point.
    void *heap_start = page_stack_allocate();
    num_of_kernel_pages--;
    kernel_heap = heap_create(heap_start, KERNEL_HEAP_SIZE);

    // All pages at this stage should be contiguous.
    while (num_of_kernel_pages-- > 0) {
        page_stack_allocate();
    }
}

/**
 * @brief Allocates a segment of memory for kernel use.
 * @param size Size of the segment to allocate.
 * @returns Starting address of the requested segment or NULL on failure.
 */
void *kmalloc(size_t size) { return heap_allocate_segment(kernel_heap, size); }

/**
 * @brief Releases a segment of memory back into the memory pool.
 * @param address Starting address of the segment to release.
 */
void kfree(void *address) { heap_deallocate_segment(kernel_heap, address); }
