/**
 * @file fopen.h
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief An interface for opening and creating file descriptors.
 *
 * An interface for opening and creating file descriptors.
 */

#include "fopen.h"
#include "kernel/syscall/printf.h"

// TODO: When file systems work this will change.
/**
 * @brief Creates an empty file desciptor with the specified mode.
 * @param mode Mode to open file in.
 * @returns An empty file descriptor.
 */
FILE *create_file(FILE_MODE mode) {
    int file_slot = get_next_file_slot();
    if (file_slot < 0) {
        printf("Unable to open file\n");
        return NULL;
    }

    FILE *file = &opened_files[file_slot];
    file->opened = 1;
    return file;
}