/**
 * @file console.h
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief Creates and updates a video based tty console.
 *
 * This creates a video based tty console from the first display driver that
 * is found on the system. An alternative driver can be used by manually
 * creating a GenericDisplay and passing that to the functions.
 */

#ifndef _NEUTRINO_KERNEL_CONSOLE_CONSOLE_H_
#define _NEUTRINO_KERNEL_CONSOLE_CONSOLE_H_

#include <stdint.h>

#include "drivers/interfaces/generic_display.h"
#include "kernel/stdio/file.h"

#define FG_COLOR 0xFFFFFFFF
#define BG_COLOR 0xFF202020

typedef struct {
    uint32_t cursor_x;              /**< X coordinate of cursor. */
    uint32_t cursor_y;              /**< Y coordinate of cursor. */
    uint32_t height;                /**< Height of the console in characters. */
    uint32_t width;                 /**< Width of the console in characters. */
    GenericDisplay *display_driver; /**< Display drivers to draw console. */
} Console;

/**
 * @brief Creates a console file descriptor from the default display driver.
 * @returns A file descriptor for the new console.
 */
FILE *create_console();

/**
 * @brief Safely closes the console and frees the memory.
 * @param console Console file descriptor to be freed.
 */
void delete_console(FILE *console);

/**
 * @brief Draws the background color to the entire display.
 * @param console Console file descriptor to draw background on.
 */
void console_clear(FILE *console);

/**
 * @brief Write a string to the console.f
 * @param buffer Characters to write to the console.
 * @param length Number of characters to write not including
 * null-terminator.
 * @param console Console to write to.
 */
void console_write(char *buffer, size_t length, FILE *console);

#endif
