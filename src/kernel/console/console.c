/**
 * @file console.c
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief Creates and updates a video based tty console.
 *
 * This creates a video based tty console from the first display driver that
 * is found on the system. An alternative driver can be used by manually
 * creating a GenericDisplay and passing that to the functions.
 *
 * TODO: Implement scrolling.
 * TODO: Implement input.
 * TODO: Implement cursor.
 * TODO: Implement coloring of text.
 * TODO: Initialize black and white and enable customization.
 */

#include "console.h"
#include "drivers/device_list.h"
#include "kernel/syscall/alloc.h"
#include "kernel/syscall/fopen.h"
#include "kernel/syscall/printf.h"
#include "pcscreenfont.h"

/**
 * @brief Creates a console file descriptor from the default display driver.
 * @returns A file descriptor for the new console.
 */
FILE *create_console() {
    GenericDisplay *graphics_driver =
        (GenericDisplay *)get_default_driver(DI_GENERIC_DISPLAY);
    if (graphics_driver == NULL) {
        printf("Unable to open display driver\n");
        return NULL;
    }

    Console *console = (Console *)kmalloc(sizeof(Console));
    console->cursor_x = 0;
    console->cursor_y = 0;
    console->width = graphics_driver->width / PSF_HEADER.width;
    console->height = graphics_driver->height / PSF_HEADER.height;
    console->display_driver = graphics_driver;

    FILE *console_file = create_file(FM_WRITE_ONLY);

    console_file->write_file = console_write;
    console_file->custom_control = console;

    return console_file;
}

/**
 * @brief Safely closes the console and frees the memory.
 * @param console Console file descriptor to be freed.
 */
void delete_console(FILE *console) {
    kfree(console->custom_control);
    console->opened = 0;
}

/**
 * @brief Draws the background color to the entire display.
 * @param console Console file descriptor to draw background on.
 */
void console_clear(FILE *console) {
    Console *console_control = (Console *)console->custom_control;
    GenericDisplay *display = console_control->display_driver;

    for (int y = 0; y < display->height; ++y) {
        for (int x = 0; x < display->width; ++x) {
            display->draw_pixel(x, y, BG_COLOR, display);
        }
    }
}

/**
 * @brief Draws a character to the console then increments the cursor.
 * @param c Character to draw.
 * @param console Console to draw to.
 */
static void console_draw_ascii(char c, Console *console) {
    uint32_t x_offset = console->cursor_x * PSF_HEADER.width;
    uint32_t y_offset = console->cursor_y * PSF_HEADER.height;
    uint32_t char_index = PSF_HEADER.bytesperglyph * (int)c;
    for (int i = 0; i < PSF_HEADER.height; ++i) {
        uint32_t mask = 1 << (PSF_HEADER.width - 1);

        for (int j = 0; j < PSF_HEADER.width; ++j) {
            uint32_t color =
                ((PSF_GLYPHS[char_index + i] & mask) > 0) ? FG_COLOR : BG_COLOR;
            console->display_driver->draw_pixel(x_offset + j, y_offset + i,
                                                color, console->display_driver);
            mask >>= 1;
        }
    }
}

/**
 * @brief Write a string to the console.f
 * @param buffer Characters to write to the console.
 * @param length Number of characters to write not including null-terminator.
 * @param console Console to write to.
 */
void console_write(char *buffer, size_t length, FILE *console) {
    Console *console_control = (Console *)console->custom_control;
    for (size_t i = 0; i < length; ++i) {
        if (buffer[i] == '\n') {
            ++console_control->cursor_y;
            console_control->cursor_x = -1;
        } else {
            console_draw_ascii(buffer[i], console_control);
        }

        if (++console_control->cursor_x > console_control->width) {
            if (++console_control->cursor_y > console_control->height) {
                // TODO: scroll
                console_control->cursor_y--;
            }
            console_control->cursor_x = 0;
        }
    }
}
