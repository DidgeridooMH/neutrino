/**
 * @file pcscreenfont.h
 * @author Daniel Simpkins
 * @date 2 Feb 2020
 * @brief Structures for accessing PC Screen Font headers.
 *
 * This file gives access to the PC Screen Font file that is loaded within the
 * kernel at compile time. The header aligns with the beginning of the file. All
 * data following the header are characters.
 */

#ifndef _NEUTRINO_KERNEL_CONSOLE_PCSCREENFONT_H_
#define _NEUTRINO_KERNEL_CONSOLE_PCSCREENFONT_H_

#include <stdint.h>

typedef struct {
    uint32_t magic;
    uint32_t version;
    uint32_t headersize;
    uint32_t flags;
    uint32_t numglyph;
    uint32_t bytesperglyph;
    uint32_t height;
    uint32_t width;
} __attribute__((packed)) PsfHeader;

/**
 * This section is loaded into the binary at compile time.
 */
extern volatile PsfHeader _binary_fonts_font_psf_start;

#define PSF_HEADER _binary_fonts_font_psf_start
#define PSF_GLYPHS \
    (((uint8_t*)&_binary_fonts_font_psf_start) + sizeof(PsfHeader))

#endif
