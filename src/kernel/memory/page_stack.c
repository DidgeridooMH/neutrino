/**
 * @file page_stack.c
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief The page stack is a stack containing all available pages in RAM.
 *
 * The page stack is a stack containing all available pages in RAM. A page is 4K
 * in size. To allocate a page, its address is popped off the stack and to
 * deallocate, the page is pushed back onto the stack.
 */

#include "page_stack.h"
#include "../../bsp/rpi/atag.h"
#include "../panic.h"
#include "../syscall/printf.h"

/** Ending address of the kernel. */
extern uint32_t __end;

/* TODO: Wrap in a lock. */
static FreePageStack g_free_page_stack = {0, 0};

/**
 * @brief Pushes an address onto the page stack.
 * @param address 4K aligned address to push.
 */
static void page_stack_push(uint32_t address) {
    uint32_t next_index = g_free_page_stack.size++;
    g_free_page_stack.base[next_index] = address;
}

/**
 * @brief Pops an address off the page stack.
 * @returns 4K aligned address off the top of the stack.
 */
static uint32_t page_stack_pop() {
    uint32_t prev_index = --g_free_page_stack.size;
    return g_free_page_stack.base[prev_index];
}

/**
 * @brief Initializes the global page stack to contain all available memory.
 */
void page_stack_initialize() {
    size_t memory_size = get_memory_size();
    if (memory_size == 0) {
        panic("Unable to get memory size");
    }

    printf("System Memory: %u MB\n", memory_size / 1024 / 1024);

    g_free_page_stack.base = &__end;

    uint32_t next_page = memory_size;
    while ((uint64_t)next_page - PAGE_SIZE >
           (uint64_t)(g_free_page_stack.base + g_free_page_stack.size)) {
        page_stack_push(next_page);
        next_page -= PAGE_SIZE;
    }
}

/**
 * @brief Allocates a page of memory.
 * @returns The starting address of the allocated page.
 */
void *page_stack_allocate() { return (void *)(uint64_t)page_stack_pop(); }

/**
 * @brief Deallocates a page of memory.
 * @param address 4K aligned page to deallocate.
 */
void page_stack_deallocate(void *address) {
    page_stack_push((uint32_t)(uint64_t)address);
}
