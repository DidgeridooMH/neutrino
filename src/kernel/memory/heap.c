/**
 * @file heap.c
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief A heap for allocating memory at a high granularity.
 *
 * A linked list implementation of a heap that allows for high granularity
 * memory allocation. Most memory allocations are rounded to the nearest 8
 * byte address to avoid hardware panics on some systems.
 */

#include "heap.h"
#include "../panic.h"

/**
 * @brief Initializes a heap starting at a specified address.
 * @param address 8 byte aligned address to initialize heap at.
 * @param size Size of the heap.
 */
HeapHeader *heap_create(void *address, size_t size) {
    HeapHeader *header = (HeapHeader *)address;
    header->size = size - sizeof(HeapHeader);
    header->next = NULL;
    header->prev = NULL;
    header->allocated = 0;
    return header;
}

/**
 * @brief Splits a segment into a segment of a desired size and the other with
 *        the remaining bytes.
 * @param header Header of the segment to split.
 * @param size Desired size of the first segment.
 */
static void split_segment(HeapHeader *header, size_t size) {
    /// Segment must be 2 times as big as the heap header to split.
    if (header->size - size >= 2 * sizeof(HeapHeader)) {
        HeapHeader *new_header = (HeapHeader *)((void *)(header + 1) + size);
        new_header->allocated = 0;
        new_header->size = header->size - size - sizeof(HeapHeader);

        new_header->next = header->next;
        header->next = new_header;
        new_header->prev = header;
        header->next->prev = new_header;
    }
}

/**
 * @brief Allocates a segment of memory in the heap.
 * @param header Starting heap header to look for a memory segment.
 * @param size Desired size of the memory segment.
 * @returns An 8-byte aligned starting address of the allocated segment.
 */
void *heap_allocate_segment(HeapHeader *header, size_t size) {
    /// Round the address to the nearest 8-bytes.
    if (size % MEMORY_ALIGNMENT > 0) {
        size += MEMORY_ALIGNMENT - size % MEMORY_ALIGNMENT;
    }

    while (header != NULL) {
        if (!header->allocated && header->size >= size) {
            split_segment(header, size);
            header->allocated = 1;
            return (void *)header + sizeof(HeapHeader);
        }
        header = header->next;
    }

    return NULL;
}

/**
 * @brief Attempt to merge a segment into the one in front of it.
 * @param header Header to attempt merge on.
 */
static void forward_merge_segments(HeapHeader *header) {
    HeapHeader *next_header = header->next;

    /// Next segment must exist and be free.
    if (next_header != NULL && !next_header->allocated) {
        header->size += next_header->size + sizeof(HeapHeader);
        header->next = next_header->next;
        if (header->next != NULL) {
            header->next->prev = header;
        }
    }
}

/**
 * @brief Attempts to merge a free segment with the segments around it.
 * @param header Header of the segment to merge.
 */
static void merge_segments(HeapHeader *header) {
    forward_merge_segments(header);

    if (header->prev != NULL && !header->prev->allocated) {
        forward_merge_segments(header->prev);
    }
}

/**
 * @brief Deallocates a segment of memory in the heap.
 * @param header Starting heap header to look for memory segment.
 * @param address Starting address of the segment to deallocate.
 */
void heap_deallocate_segment(HeapHeader *header, void *address) {
    address -= sizeof(HeapHeader);
    if (!header->allocated) {
        panic("Double free exception!");
    }
    header->allocated = 0;
    merge_segments(header);
}
