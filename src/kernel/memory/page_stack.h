/**
 * @file page_stack.h
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief The page stack is a stack containing all available pages in RAM.
 *
 * The page stack is a stack containing all available pages in RAM. A page is 4K
 * in size. To allocate a page, its address is popped off the stack and to
 * deallocate, the page is pushed back onto the stack.
 */

#ifndef _NEUTRINO_KERNEL_MEMORY_PAGE_STACK_H_
#define _NEUTRINO_KERNEL_MEMORY_PAGE_STACK_H_

#include <stdint.h>

/**
 * Most systems use a 4K page division. However, some 64-bit systems will allow
 * for less granular 4M page divisions.
 */
#define PAGE_SIZE 4096ul

typedef struct {
    uint32_t *base;
    uint32_t size;
} FreePageStack;

/**
 * @brief Initializes the global page stack to contain all available memory.
 */
void page_stack_initialize();

/**
 * @brief Allocates a page of memory.
 * @returns The starting address of the allocated page.
 */
void *page_stack_allocate();

/**
 * @brief Deallocates a page of memory.
 * @param address 4K aligned page to deallocate.
 */
void page_stack_deallocate(void *address);

#endif
