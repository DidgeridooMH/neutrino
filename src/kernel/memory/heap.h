/**
 * @file heap.h
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief A heap for allocating memory at a high granularity.
 *
 * A linked list implementation of a heap that allows for high granularity
 * memory allocation. Most memory allocations are rounded to the nearest 8
 * byte address to avoid hardware panics on some systems.
 */

#ifndef _NEUTRINO_KERNEL_MEMORY_HEAP_H_
#define _NEUTRINO_KERNEL_MEMORY_HEAP_H_

#include <stddef.h>

#define MEMORY_ALIGNMENT 8

struct HeapHeader {
    size_t size;
    int allocated;
    struct HeapHeader *next;
    struct HeapHeader *prev;
};
typedef struct HeapHeader HeapHeader;

/**
 * @brief Initializes a heap starting at a specified address.
 * @param address 8 byte aligned address to initialize heap at.
 * @param size Size of the heap.
 */
HeapHeader *heap_create(void *address, size_t size);

/**
 * @brief Allocates a segment of memory in the heap.
 * @param header Starting heap header to look for a memory segment.
 * @param size Desired size of the memory segment.
 * @returns An 8-byte aligned starting address of the allocated segment.
 */
void *heap_allocate_segment(HeapHeader *header, size_t size);

/**
 * @brief Deallocates a segment of memory in the heap.
 * @param header Starting heap header to look for memory segment.
 * @param address Starting address of the segment to deallocate.
 */
void heap_deallocate_segment(HeapHeader *header, void *address);

#endif
