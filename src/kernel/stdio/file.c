/**
 * @file file.c
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief An early stage implementation of a file interface.
 *
 * This is an early stage implementation of a file interface. It currently is
 * just a generic I/O interface. When the kernel file services are implemented
 * this will be more fleshed out.
 *
 * TODO: Refine this system and create interfaces for creating files.
 */

#include "file.h"

/**
 * Global variable for all the opened_files in the system.
 */
FILE opened_files[MAX_FILES];

/**
 * @brief Retrieves the next available file slot.
 * @returns The index of the next available file slot.
 */
int get_next_file_slot() {
    for (int i = 0; i < MAX_FILES; ++i) {
        if (!opened_files[i].opened) {
            return i;
        }
    }

    return -1;
}
