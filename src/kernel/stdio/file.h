/**
 * @file file.h
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief An early stage implementation of a file interface.
 *
 * This is an early stage implementation of a file interface. It currently is
 * just a generic I/O interface. When the kernel file services are implemented
 * this will be more fleshed out.
 *
 * TODO: Refine this system and create interfaces for creating files.
 */

#ifndef _NEUTRINO_KERNEL_STDIO_FILE_H_
#define _NEUTRINO_KERNEL_STDIO_FILE_H_

#include <stddef.h>

/**
 * This is an arbitrary number and will probably grow in later stages.
 */
#define MAX_FILES 16

/**
 * Default files for programs to interface with.
 */
#define stdout (&opened_files[0])
#define stderr (&opened_files[1])
#define stdin (&opened_files[2])

struct FILE;
typedef size_t (*fp_file_read)(char* buffer, struct FILE* file);
typedef void (*fp_file_write)(char* buffer, size_t length, struct FILE* file);

struct FILE {
    fp_file_read read_file;
    fp_file_write write_file;

    int opened;

    void* custom_control;
};
typedef struct FILE FILE;

/**
 * Global variable for all the opened_files in the system.
 */
extern FILE opened_files[];

/**
 * @brief Retrieves the next available file slot.
 * @returns The index of the next available file slot.
 */
int get_next_file_slot();

#endif
