/**
 * @file panic.c
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief An error reporter for when the kernel can no longer continue.
 *
 * This component contains a set of kernel error routines. Its purpose is to
 * give final information on why the system had to halt.
 *
 * TODO: Add register dumping and error codes.
 */

#include "panic.h"
#include "syscall/printf.h"

/**
 * @brief Report an error that caused the kernel to stop.
 *
 * @param message A message to display before halting the system.
 */
void panic(const char *message) {
    printf("KERNEL PANIC: %s\n", message);
    while (1) {
        asm volatile("nop");
    }
}
