/**
 * @file kernel.c
 * @author Daniel Simpkins
 * @date 1 Feb 2020
 * @brief Initialization of essential services, drivers, and early-stage logs.
 *
 * This file initializes an early-stage logging mechanism for the device
 * (i.e. UART1 on the RPi). It then begins to initialization of essential
 * systems such as the device driver pool and system memory management, then
 * attempts to initialize a more robust logging console.
 */

#include "console/console.h"
#include "memory/page_stack.h"
#include "stdio/file.h"
#include "syscall/alloc.h"
#include "syscall/printf.h"

/*
 *  TODO: Generalize this such that architecture can initialize their drivers
 *        somewhere other than in the kernel.
 */
#if __DEVICE_TARGET == RASPI
#include "../bsp/rpi/uart.h"
#include "bsp/rpi/drivers/devices.h"
#endif

/*
 * Bootstrap the build number system. This should never happen unless something
 * went wrong on source control or the make system.
 */
#ifndef __BUILD_NUMBER
#define __BUILD_NUMBER 0
#endif

// TODO: Change build system to simply ignore the kernel.c file.
#ifndef TEST_BUILD

/**
 * @brief The "entry point" of the OS after the bootloader. This initializes
 *        essential kernel mechanisms and starts the proper services to allow
 *        for usermode.
 */
void kmain() {
    uart_initialize();

    while (uart_getc() != '\n') {
        asm volatile("nop");
    }

    page_stack_initialize();
    kernel_heap_initialize();

    generate_device_drivers();

    FILE *console = create_console();
    console_clear(console);
    printf("Welcome to Neutrino!\nBuild 0x%x\n", __BUILD_NUMBER);
}

#endif
